/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.node;

import pl.edu.agh.age.services.lifecycle.NodeLifecycleService;
import pl.edu.agh.age.util.NodeSystemProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Bootstrapper for the node.
 *
 * A node existence is bound to the **NodeLifecycleService** and the node is destroyed when this service is stopped.
 *
 * @see NodeLifecycleService
 */
@SuppressWarnings("CallToSystemExit")
public final class NodeBootstrapper {

	static {
		System.setProperty("logback.statusListenerClass", "ch.qos.logback.core.status.NopStatusListener");
	}

	private static final Logger logger = LoggerFactory.getLogger(NodeBootstrapper.class);

	private NodeBootstrapper() {}

	public static void main(final String... args) throws InterruptedException {
		final String configName = NodeSystemProperties.CONFIG.get();
		try (final ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(configName)) {
			context.registerShutdownHook();
			final NodeLifecycleService lifecycleService = context.getBean(NodeLifecycleService.class);
			if (lifecycleService == null) {
				logger.error("No node lifecycle service is defined");
				return;
			}
			lifecycleService.awaitTermination();
		}
		logger.info("Exiting");
		System.exit(0);
	}
}
