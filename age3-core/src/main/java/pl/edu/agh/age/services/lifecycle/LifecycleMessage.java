/*
 * Copyright (C) 2016-2018 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */
package pl.edu.agh.age.services.lifecycle;

import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Optional;

import javax.annotation.Nullable;

public final class LifecycleMessage implements Serializable {

	public enum Type {
		DESTROY(false);

		private final boolean payloadRequired;

		Type(final boolean payloadRequired) {
			this.payloadRequired = payloadRequired;
		}

		public boolean isPayloadRequired() {
			return payloadRequired;
		}
	}

	private static final long serialVersionUID = -5867847961864763792L;

	private final Type type;

	private final @Nullable Serializable payload;

	private LifecycleMessage(final Type type, final @Nullable Serializable payload) {
		this.type = requireNonNull(type);
		this.payload = payload;
	}

	public static LifecycleMessage createWithoutPayload(final Type type) {
		checkArgument(!type.isPayloadRequired(), "Message type require payload.");
		return new LifecycleMessage(type, null);
	}

	public Type type() {
		return type;
	}

	public boolean hasType(final Type typeToCheck) {
		return type == requireNonNull(typeToCheck);
	}

	public Optional<Serializable> payload() {
		return Optional.ofNullable(payload);
	}

	@Override public String toString() {
		return toStringHelper(this).add("type", type).addValue(payload).toString();
	}
}
