/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.services.worker.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;

import javax.annotation.Nullable;

/*
 * This is not-so-good solution to the problem of passing data from the compute through the Hazelcast utilities that run
 * in another thread (for example https://gitlab.com/age-agh/age3/issues/58). The problem occurs when the custom class
 * is unserialized in the Hazelcast thread. In these cases we need to use the correct classloader and our own
 * serialization.
 *
 * The better solution would be to process everything in the compute thread, but it would need some heavy refactoring.
 */
final class Serializer {
	private static final Logger log = LoggerFactory.getLogger(Serializer.class);

	private Serializer() {}

	static byte[] serialize(final Serializable message) throws IOException {
		try (ByteArrayOutputStream byos = new ByteArrayOutputStream();
		     ObjectOutputStream oos = new ObjectOutputStream(byos)) {
			oos.writeObject(message);
			return byos.toByteArray();
		}
	}

	static Serializable unserialize(final byte[] message, final @Nullable ClassLoader classLoader)
		throws IOException, ClassNotFoundException {
		try (ObjectInputStream ois = new CustomClassLoaderObjectInputStream(new ByteArrayInputStream(message),
		                                                                    classLoader)) {
			return (Serializable)ois.readObject();
		}
	}

	static final class CustomClassLoaderObjectInputStream extends ObjectInputStream {

		private final @Nullable ClassLoader classLoader;

		CustomClassLoaderObjectInputStream(final InputStream in, final @Nullable ClassLoader classLoader)
			throws IOException {
			super(in);
			this.classLoader = classLoader;
		}

		@Override protected Class<?> resolveClass(final ObjectStreamClass desc)
			throws IOException, ClassNotFoundException {
			try {
				return Class.forName(desc.getName(), false, classLoader);
			} catch (final ClassNotFoundException e) {
				log.error("Class {} not found in the COMPUTE classloader, delegating to the node", desc.getName());
				return super.resolveClass(desc);
			}
		}
	}
}
