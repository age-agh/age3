/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import static jcuda.driver.JCudaDriver.cuCtxCreate;
import static jcuda.driver.JCudaDriver.cuCtxDestroy;
import static jcuda.driver.JCudaDriver.cuCtxSetCurrent;
import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuDeviceGet;
import static jcuda.driver.JCudaDriver.cuInit;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;
import static jcuda.driver.JCudaDriver.cuMemAlloc;
import static jcuda.driver.JCudaDriver.cuMemFree;
import static jcuda.driver.JCudaDriver.cuMemGetInfo;
import static jcuda.driver.JCudaDriver.cuMemcpyDtoH;
import static jcuda.driver.JCudaDriver.cuMemcpyHtoD;
import static jcuda.driver.JCudaDriver.cuModuleGetFunction;
import static jcuda.driver.JCudaDriver.cuModuleLoad;
import static jcuda.driver.JCudaDriver.cuModuleUnload;

import pl.edu.agh.age.compute.labs.evaluator.LabsEnergy;
import pl.edu.agh.age.compute.labs.solution.LabsSolution;

import one.util.streamex.StreamEx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import io.vavr.collection.Array;
import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUcontext;
import jcuda.driver.CUdevice;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import jcuda.driver.JCudaDriver;

final class DeviceExecutor {

	private static final Logger log = LoggerFactory.getLogger(DeviceExecutor.class);

	private static final int maxNumberOfBlocks = 768;

	private CUfunction function;

	private CUmodule module;

	private CUdevice device;

	private CUcontext context;

	private final String ptxFileName;

	private CUdeviceptr deviceInputA;

	private CUdeviceptr deviceNumberOfRepeating;

	private CUdeviceptr devResult;

	private short[] hostInputA;

	private final boolean modifyEvaluatedSolution;

	private final int lengthOfSequence;

	private final int numberOfThreads;

	private final int numberOfRepeating;

	private long kernelLaunches = 0;

	public DeviceExecutor(final String cudaFilePath, final boolean modifyEvaluatedSolution, final int lengthOfSequence,
	                      final int numberOfThreads, final int numberOfRepeating) {
		this.modifyEvaluatedSolution = modifyEvaluatedSolution;
		this.lengthOfSequence = lengthOfSequence;
		this.numberOfThreads = numberOfThreads;
		this.numberOfRepeating = numberOfRepeating;

		// Enable exceptions and omit all subsequent error checks
		JCudaDriver.setExceptionsEnabled(true);

		ptxFileName = preparePtxFile(cudaFilePath);

		initializeDevices();
	}

	public List<ComparableAgent> execute(final List<ComparableAgent> cache) {
		final int numberOfBlocks = cache.size();
		hostInputA = allocateInputData(cache);

		cuCtxSetCurrent(context);
		// Copy the host input data to the device
		cuMemcpyHtoD(deviceInputA, Pointer.to(hostInputA), numberOfThreads * numberOfBlocks * Sizeof.SHORT);

		final Pointer kernelParameters = Pointer.to(Pointer.to(deviceInputA), Pointer.to(devResult),
		                                            Pointer.to(deviceNumberOfRepeating));

		kernelLaunches++;
		log.debug("Launching kernel no {} for {} agents", kernelLaunches, numberOfBlocks);
		final Instant start = Instant.now();
		// Call the kernel function.
		cuLaunchKernel(function, numberOfBlocks, 1, 1, // Grid dimension
		               numberOfThreads, 1, 1, // Block dimension
		               0, null, // Shared memory size and stream
		               kernelParameters, null // Kernel- and extra parameters
		              );
		cuCtxSynchronize();
		final Instant end = Instant.now();

		// Allocate host output memory and copy the device output
		// to the host.
		final int[] energy = new int[numberOfBlocks];
		cuMemcpyDtoH(Pointer.to(energy), devResult, numberOfBlocks * Sizeof.INT);
		log.debug("After kernel and copy – call took {} ms", Duration.between(start, end).toMillis());

		final List<ComparableAgent> newPopulation;

		if (modifyEvaluatedSolution) {
			final short[] sequence = new short[lengthOfSequence * numberOfBlocks];
			cuMemcpyDtoH(Pointer.to(sequence), deviceInputA, lengthOfSequence * numberOfBlocks * Sizeof.SHORT);
			//verifyGPUResults(energy, sequence);
			newPopulation = createOutputPopulation(cache, sequence, energy);
		} else {
			newPopulation = createOutputPopulation(cache, energy);
		}

		return newPopulation;
	}

	/**
	 * The extension of the given file name is replaced with "ptx". If the file with
	 * the resulting name does not exist, it is compiled from the given file using
	 * NVCC. The name of the PTX file is returned.
	 *
	 * @param cuFileName
	 * 		The name of the .CU file
	 *
	 * @return The name of the PTX file
	 *
	 * @throws IOException
	 * 		If an I/O error occurs
	 */
	private String preparePtxFile(final String cuFileName) {
		int endIndex = cuFileName.lastIndexOf('.');
		if (endIndex == -1) {
			endIndex = cuFileName.length() - 1;
		}
		String ptxFileName = cuFileName.substring(0, endIndex + 1) + "ptx";

		// if (ptxFile.exists())
		// {
		// return ptxFileName;
		// }

		File cuFile = new File(cuFileName);
		if (!cuFile.exists()) {
			throw new RuntimeException("Input file not found: " + cuFileName);
		}
		String modelString = "-m" + System.getProperty("sun.arch.data.model");
		System.out.println("model stirng is " + modelString);
		String command = "nvcc -gencode arch=compute_35,code=sm_35 "
		                 + modelString
		                 + " -ptx "
		                 + cuFile.getPath()
		                 + " -o "
		                 + ptxFileName;

		System.out.println("Executing\n" + command);
		try {
			Process process = Runtime.getRuntime().exec(command);

			String errorMessage = new String(toByteArray(process.getErrorStream()));
			String outputMessage = new String(toByteArray(process.getInputStream()));

			int exitValue = 0;
			exitValue = process.waitFor();
			if (exitValue != 0) {
				System.out.println("nvcc process exitValue " + exitValue);
				System.out.println("errorMessage:\n" + errorMessage);
				System.out.println("outputMessage:\n" + outputMessage);
				throw new RuntimeException("Could not create .ptx file: " + errorMessage);
			}
		} catch (IOException | InterruptedException e) {
			throw new RuntimeException(e);
		}

		System.out.println("Finished creating PTX file");
		return ptxFileName;
	}

	private void initializeDevices() {
		// Initialize the driver and create a context for the first device.
		cuInit(0);
		device = new CUdevice();
		cuDeviceGet(device, 0);

		context = new CUcontext();
		cuCtxCreate(context, 0, device);

		module = new CUmodule();
		cuModuleLoad(module, ptxFileName);

		function = new CUfunction();
		cuModuleGetFunction(function, module, "kernel");

		hostInputA = new short[numberOfThreads * maxNumberOfBlocks];
		deviceInputA = new CUdeviceptr();
		cuMemAlloc(deviceInputA, numberOfThreads * maxNumberOfBlocks * Sizeof.SHORT);

		deviceNumberOfRepeating = new CUdeviceptr();
		cuMemAlloc(deviceNumberOfRepeating, Sizeof.INT);

		// Allocate device output memory
		devResult = new CUdeviceptr();
		cuMemAlloc(devResult, maxNumberOfBlocks * Sizeof.INT);

		cuMemcpyHtoD(deviceNumberOfRepeating, Pointer.to(new int[] {numberOfRepeating}), Sizeof.INT);

		final long[] total = {0L};
		final long[] free = {0L};
		cuMemGetInfo(free, total);
		log.info("Memory: free {}, total {}", free[0], total[0]);
	}

	public void cleanUp() {
		log.info("Cleaning up");
		cuCtxDestroy(context);
		cuMemFree(deviceInputA);
		cuMemFree(deviceNumberOfRepeating);
		cuMemFree(devResult);
		cuModuleUnload(module);
	}

	private short[] allocateInputData(final List<ComparableAgent> population) {
		//log.debug("Agents: {}", population);

		// Allocate and fill the host input data
		final int numberOfBlocks = population.size();
		if (numberOfBlocks > maxNumberOfBlocks) {
			throw new IllegalStateException(String.format("%d, %d", numberOfBlocks, maxNumberOfBlocks));
		}

		final short[] hostInputA = new short[numberOfThreads * numberOfBlocks];
		int seqInd = 0;
		for (final ComparableAgent ind : population) {
			final Array<Boolean> solution = ((LabsSolution)ind.agent.solution).unwrap();
			for (int bit = 0; bit < numberOfThreads; bit++) {
				if (bit < solution.size()) {
					hostInputA[seqInd * numberOfThreads + bit] = (short)(solution.get(bit) ? 1 : -1);
				} else {
					hostInputA[seqInd * numberOfThreads + bit] = (short)0;
				}
			}
			seqInd++;
		}

		return hostInputA;
	}

	private List<ComparableAgent> createOutputPopulation(final List<ComparableAgent> population,
	                                                     final short[] sequences, final int[] energy) {
		final List<LabsSolution> solutions = Array.ofAll(sequences)
		                                          .sliding(lengthOfSequence, lengthOfSequence)
		                                          .zip(Array.ofAll(energy))
		                                          .map(v -> new LabsSolution(v._1).withFitness(computeFitness(v._2)))
		                                          .toJavaList();

		final List<ComparableAgent> set = StreamEx.of(population)
		                                          .zipWith(StreamEx.of(solutions))
		                                          .map(m -> new ComparableAgent(
			                                          m.getKey().agent.withSolution(m.getValue()),
			                                          m.getKey().workplace))
		                                          .toList();

		//log.debug("Agents after improvement: {}", treeMap);

		return set;
	}

	private List<ComparableAgent> createOutputPopulation(final List<ComparableAgent> population, final int[] energy) {
		final List<Double> fitness = Array.ofAll(energy).map(this::computeFitness).toJavaList();

		final List<ComparableAgent> set = StreamEx.of(population)
		                                          .zipWith(StreamEx.of(fitness))
		                                          .map(i -> new ComparableAgent(i.getKey().agent.withSolution(
			                                          i.getKey().agent.solution.withFitness(i.getValue())),
		                                                                        i.getKey().workplace))
		                                          .toList();

		//log.debug("Agents after improvement: {}", treeMap);

		return set;
	}


	/*
	 * Utility methods for test purposes
	 */
	@SuppressWarnings("unused") private boolean verifyGPUResults(int[] energy, short[] sequence) {
		for (int i = 0; i < energy.length; i++) {
			final short[] solution = Arrays.copyOfRange(sequence, i * lengthOfSequence,
			                                            (i * lengthOfSequence) + lengthOfSequence);
			final LabsSolution labsSolution = new LabsSolution(Array.ofAll(solution));
			final LabsEnergy referenceEnergy = new LabsEnergy(labsSolution);
			if (energy[i] == referenceEnergy.value()) {
				// log.info("PASSED");
			} else {
				log.error("WRONG: Solution {} has {} should have {}", labsSolution, energy[i], referenceEnergy.value());
			}
		}

		return true;
	}

	@SuppressWarnings("unused") private short[] allocateTestData(String repr, int numberOfBlocks) {
		short hostInputA[] = new short[numberOfThreads * numberOfBlocks];

		short[] sequence = fromString(repr);
		for (int seqInd = 0; seqInd < numberOfBlocks; seqInd++) {
			for (int bit = 0; bit < numberOfThreads; bit++) {
				if (bit < sequence.length) {
					hostInputA[seqInd * numberOfThreads + bit] = sequence[bit];
				} else {
					hostInputA[seqInd * numberOfThreads + bit] = (short)0;
				}
			}
		}

		return hostInputA;
	}


	private static final short[] fromString(String repr) {
		java.util.List<Short> sequenceList = new LinkedList<Short>();
		for (String elem : repr.split(" ")) {
			if (elem.equals("-1")) { sequenceList.add((short)-1); } else if (elem.equals("+1")) {
				sequenceList.add((short)1);
			}
		}
		short[] sequence = new short[sequenceList.size()];
		for (int i = 0; i < sequence.length; i++) {
			sequence[i] = sequenceList.get(i);
		}
		return sequence;
	}

	private double computeFitness(int energy) {
		return ((double)lengthOfSequence * lengthOfSequence) / (2 * energy);
	}

	/**
	 * Fully reads the given InputStream and returns it as a byte array
	 *
	 * @param inputStream
	 * 		The input stream to read
	 *
	 * @return The byte array containing the data from the input stream
	 *
	 * @throws IOException
	 * 		If an I/O error occurs
	 */
	private byte[] toByteArray(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte buffer[] = new byte[8192];
		while (true) {
			int read = inputStream.read(buffer);
			if (read == -1) {
				break;
			}
			baos.write(buffer, 0, read);
		}
		return baos.toByteArray();
	}
}
