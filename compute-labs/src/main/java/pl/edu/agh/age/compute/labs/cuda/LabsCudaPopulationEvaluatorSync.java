/*
 * Copyright (C) 2016-2019 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */

package pl.edu.agh.age.compute.labs.cuda;

import pl.edu.agh.age.compute.stream.emas.EmasAgent;
import pl.edu.agh.age.compute.stream.emas.PopulationEvaluator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;

import io.vavr.collection.List;
import io.vavr.collection.Seq;

/**
 * Created by kpietak on 17.01.2017.
 */
public final class LabsCudaPopulationEvaluatorSync implements PopulationEvaluator<EmasAgent> {

	private static final Logger log = LoggerFactory.getLogger(LabsCudaPopulationEvaluatorSync.class);

	private final LabsCudaPopulationEvaluatorSyncOnDevice globalEvaluator;

	public LabsCudaPopulationEvaluatorSync(final LabsCudaPopulationEvaluatorSyncOnDevice globalEvaluator) {
		this.globalEvaluator = globalEvaluator;
	}

	@Override public Seq<EmasAgent> evaluate(final Seq<EmasAgent> population) {
		if (population.isEmpty()) {
			return population;
		}

		log.debug("[{}], Sending population of {} individuals to device evaluator", hashCode(), population.size());
		final Instant start = Instant.now();
		final Seq<EmasAgent> newPopulation;
		try {
			newPopulation = globalEvaluator.evaluate(hashCode(), population.toSet()).toList();
		} catch (final InterruptedException e) {
			log.debug("Interrupted", e);
			return List.empty();
		}
		final Instant end = Instant.now();
		log.debug("[{}] Received population of {} individuals from device evaluator – took {} ms", hashCode(),
		          newPopulation.size(), Duration.between(start, end).toMillis());
		return newPopulation;
	}

}
