extern "C"
#include <thrust/sort.h>
#define numberOfThreads 64
#define lenghtOfSequence 64
#define sizeOfTauTable (lenghtOfSequence-1)*lenghtOfSequence/2
__device__ int posmin(int volatile *sdata, int id1, int id2) {
	if (thrust::min(sdata[id1], sdata[id2]) == sdata[id1])
		return id1;
	else
		return id2;
}

__device__ void reduce(int *g_data) {

	__shared__ volatile int sdata[2 * numberOfThreads];
	unsigned int tid = threadIdx.x;
	sdata[tid] = g_data[tid];
	sdata[tid + blockDim.x] = g_data[tid + blockDim.x];

	int posv;
	//if (tid == 0) 
	//   ("dane %d\n", sdata[512+257]);

	for (unsigned int s = blockDim.x / 2; s>32; s >>= 1) {
		if (tid < s) {
			//sdata[tid] += sdata[tid + s];
			posv = posmin(sdata, tid, tid + s);
			sdata[tid] = thrust::min(sdata[tid], sdata[tid + s]);
			sdata[tid + blockDim.x] = sdata[blockDim.x + posv];
		}
		__syncthreads();
	}

	__syncthreads();
	if (tid < 32) {
		if (blockDim.x >= 64){
			posv = posmin(sdata, tid, tid + 32);
			sdata[tid] = thrust::min(sdata[tid], sdata[tid + 32]);
			sdata[tid + blockDim.x] = sdata[blockDim.x + posv];
		}
		//if (tid == 1) 
		//   ("pos %d\n", sdata[512+tid]);

		posv = posmin(sdata, tid, tid + 16);
		sdata[tid] = thrust::min(sdata[tid], sdata[tid + 16]);
		sdata[tid + blockDim.x] = sdata[blockDim.x + posv];

		//if (tid == 1) 
		//   ("pos %d\n", sdata[512+tid]);

		posv = posmin(sdata, tid, tid + 8);
		sdata[tid] = thrust::min(sdata[tid], sdata[tid + 8]);
		sdata[tid + blockDim.x] = sdata[blockDim.x + posv];

		//if (tid == 1) 
		//   ("pos %d\n", sdata[512+tid]);

		posv = posmin(sdata, tid, tid + 4);
		sdata[tid] = thrust::min(sdata[tid], sdata[tid + 4]);
		sdata[tid + blockDim.x] = sdata[blockDim.x + posv];

		//if (tid == 1) 
		//   ("pos %d\n", sdata[512+tid]);

		posv = posmin(sdata, tid, tid + 2);
		sdata[tid] = thrust::min(sdata[tid], sdata[tid + 2]);
		sdata[tid + blockDim.x] = sdata[blockDim.x + posv];

		//if (tid == 1) 
		//   ("pos %d\n", sdata[512+tid]);

		posv = posmin(sdata, tid, tid + 1);
		sdata[tid] = thrust::min(sdata[tid], sdata[tid + 1]);
		sdata[tid + blockDim.x] = sdata[blockDim.x + posv];

	}

	__syncthreads();

	g_data[0] = sdata[0];
	g_data[1] = sdata[blockDim.x];
	__syncthreads();

}



__device__ void warpReduce(volatile int *data, int tid){

	if (blockDim.x >= 64) data[tid] += data[tid + 32];
	if (blockDim.x >= 32) data[tid] += data[tid + 16];
	if (blockDim.x >= 16) data[tid] += data[tid + 8];
	if (blockDim.x >= 8) data[tid] += data[tid + 4];
	if (blockDim.x >= 4) data[tid] += data[tid + 2];
	if (blockDim.x >= 2) data[tid] += data[tid + 1];
}

__device__ void reduceOnSharedMemory(volatile int *in, volatile int * out) {
	unsigned int tid = threadIdx.x;
	for (unsigned j = blockDim.x / 2; j> 32; j >>= 1){
		if (tid < j) {
			in[tid] += in[tid + j];
		}
		__syncthreads();
	}
	if (tid < 32) warpReduce(in, tid);


	out[0] = in[0];

}

__device__  void calculateCp(const volatile short *in, volatile int distance, volatile unsigned int maxIdxOfTh, volatile int * out) {
	__shared__   volatile int subset[numberOfThreads];
	unsigned int tid = threadIdx.x;
	subset[tid] = 0;
	__syncthreads();

	if (tid <= maxIdxOfTh){
		subset[tid] = in[tid] * in[tid + distance];

	}
	__syncthreads();
	reduceOnSharedMemory(subset, out);
}

__device__ void  calculeteFirstEergy(const volatile short  * in, int  * SharedsumCp, int * out){
	__shared__ volatile int result[1];
	for (int i = 0; i < lenghtOfSequence - 1; i++) {
		calculateCp(in, i + 1, lenghtOfSequence - i - 2, result);
		__syncthreads();
		if (threadIdx.x == 0) {
			SharedsumCp[i] = result[0];
			out[0] += (result[0] * result[0]);
		}
		__syncthreads();
	}
}


__device__ void createTauTab(volatile short *in, volatile short *subset, int numberOfData) {
	//Sum of arthemtic series
	int maxIdxOfTh = numberOfData - 1;
	for (int i = 0; i < numberOfData; i++) {
		if (threadIdx.x <= (maxIdxOfTh)) {
			subset[threadIdx.x + i + i*numberOfData - i*(1 + i) / 2] = in[threadIdx.x] * in[threadIdx.x + i + 1];
		}
		maxIdxOfTh = maxIdxOfTh - 1;
		__syncthreads();
	}
	__syncthreads();
}

struct ResultCpAndEnergy{
	int f;
	int sumCp[lenghtOfSequence - 1];
};



__device__ ResultCpAndEnergy  calculateEnergyAfterChangeBit(const volatile short * SharedMemoryTauResultTable, volatile  int * SharedsumCp){
	int f = 0;
	int v = 0;
	ResultCpAndEnergy result;

	for (int p = 0; p < lenghtOfSequence - 1; p++) {
		v = SharedsumCp[p];
		if ((p + 1) <= (lenghtOfSequence - (threadIdx.x + 1))){
			v = v - 2 * SharedMemoryTauResultTable[threadIdx.x + p + p*(lenghtOfSequence - 1) - p*(1 + p) / 2];
		}

		if ((p + 1) < (threadIdx.x + 1)) {
			v = v - 2 * SharedMemoryTauResultTable[(threadIdx.x + p + p*(lenghtOfSequence - 1) - p*(1 + p) / 2) - (p + 1)];
		}


		result.sumCp[p] = v;
		f = f + v*v;
	}


	result.f = f;
	return result;
}

__device__ void updateTabu(volatile short *SharedMemoryTauResultTable, int indexOfThread) {
	//Sum of arthemtic series
	for (int p = 0; p < lenghtOfSequence - 1; p++) {
		if ((p + 1) <= (lenghtOfSequence - (indexOfThread + 1))){
			SharedMemoryTauResultTable[indexOfThread + p + p*(lenghtOfSequence - 1) - p*(1 + p) / 2] *= -1;
		}

		if ((p + 1) < (indexOfThread + 1)){
			SharedMemoryTauResultTable[(indexOfThread + p + p*(lenghtOfSequence - 1) - p*(1 + p) / 2) - (p + 1)] *= -1;
		}

	}
}


extern "C"

__global__ void kernel(short *devInputSequence, int *bestPowOfSum, int *numberOfRepeating){

	__shared__ short SharedMemoryForAllSequnce[numberOfThreads];
	__shared__ int SharedsumCp[numberOfThreads - 1];
	__shared__ int firstEnergy[1];
	__shared__ short SharedMemoryTauResultTable[sizeOfTauTable];
	__shared__ int  ResultFromThreadAfterChangedBit[2 * numberOfThreads];
	__shared__	  int bestResult[1];
	//improvment[0] = true;
	firstEnergy[0] = 0;
	bestResult[0] = 0;
    int N = numberOfRepeating[0];


	SharedMemoryForAllSequnce[threadIdx.x] = devInputSequence[threadIdx.x + blockDim.x*blockIdx.x];
	__syncthreads();
	if (threadIdx.x < lenghtOfSequence) {
		createTauTab(SharedMemoryForAllSequnce, SharedMemoryTauResultTable, lenghtOfSequence - 1);
		ResultFromThreadAfterChangedBit[threadIdx.x + numberOfThreads] = threadIdx.x;
	}
	__syncthreads();
	calculeteFirstEergy(SharedMemoryForAllSequnce, SharedsumCp, firstEnergy);
	ResultFromThreadAfterChangedBit[threadIdx.x] = INT_MAX;
	


	ResultCpAndEnergy fAfterChange;
    for(int counter = 0; counter < N; counter++){
		if (threadIdx.x < lenghtOfSequence) {
			fAfterChange = calculateEnergyAfterChangeBit(SharedMemoryTauResultTable, SharedsumCp);
		}
		__syncthreads();

		if (threadIdx.x < lenghtOfSequence) {
			ResultFromThreadAfterChangedBit[threadIdx.x] = fAfterChange.f;
		}
		__syncthreads();

		reduce(ResultFromThreadAfterChangedBit);

		if (ResultFromThreadAfterChangedBit[0] < firstEnergy[0]) {
			if (threadIdx.x == ResultFromThreadAfterChangedBit[1]){
				//improvment[0] = true;
				SharedMemoryForAllSequnce[threadIdx.x] *= -1;
				for (int p = 0; p < lenghtOfSequence - 1; p++){
					SharedsumCp[p] = fAfterChange.sumCp[p];
				}
				updateTabu(SharedMemoryTauResultTable, threadIdx.x);
				firstEnergy[0] = ResultFromThreadAfterChangedBit[0];
			}
		} else {
		    bestResult[0] = firstEnergy[0];
		    break;
		}
		__syncthreads();
	}

	if (threadIdx.x == 0) {
		bestPowOfSum[blockIdx.x] = bestResult[0];
	}
	__threadfence();
	__syncthreads();
	
	

	if (threadIdx.x < lenghtOfSequence){
		devInputSequence[blockIdx.x*lenghtOfSequence + threadIdx.x] = SharedMemoryForAllSequnce[threadIdx.x];
	}
	__threadfence();
	__syncthreads();

}
