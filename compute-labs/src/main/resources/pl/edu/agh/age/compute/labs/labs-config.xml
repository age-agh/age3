<?xml version="1.0" encoding="UTF-8"?><!--
  ~ Copyright (C) 2016-2018 Intelligent Information Systems Group.
  ~
  ~ This file is part of AgE.
  ~
  ~ AgE is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ AgE is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with AgE.  If not, see <http://www.gnu.org/licenses/>.
  -->

<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd

http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

	<!-- Required to process annotations -->
	<context:annotation-config/>

	<!-- ******************* PROBLEM DEFINITION ******************* -->

	<bean id="problemDefinition" class="pl.edu.agh.age.compute.labs.problem.LabsProblem">
		<constructor-arg name="problemSize" value="${labs.problem.size}"/>
	</bean>

	<bean id="stopCondition" class="pl.edu.agh.age.compute.stream.TimedStopCondition">
		<constructor-arg value="${labs.problem.stop-condition-in-seconds}" type="long"/>
	</bean>

	<bean id="loggingInterval" class="java.time.Duration" factory-method="ofMillis">
		<constructor-arg value="${labs.problem.logging-interval-in-ms}"/>
	</bean>

	<!-- *********** EMAS OPERATORS & STEP CONFIGURATION *********** -->

	<bean id="agentsCount" class="java.lang.Integer">
		<constructor-arg value="${labs.population.size}"/>
	</bean>

	<bean id="initialAgentEnergy" class="java.lang.Double">
		<constructor-arg value="${labs.population.initial-energy}"/>
	</bean>

	<bean id="workplacesCount" class="java.lang.Integer">
		<constructor-arg value="${labs.migration.workplaces.count}"/>
	</bean>

	<bean id="migrationParameters" class="pl.edu.agh.age.compute.stream.emas.migration.MigrationParameters">
		<constructor-arg name="stepInterval" value="${labs.migration.steps-interval}"/>
		<constructor-arg name="partToMigrate" value="${labs.migration.part-to-migrate}"/>
		<constructor-arg name="migrationStrategy" value="${labs.migration.strategy}"/>
	</bean>

	<bean id="topology" class="${labs.migration.topology}"/>

	<bean id="reproductionPredicate"
	      class="pl.edu.agh.age.compute.stream.emas.Predicates"
	      factory-method="${labs.emas.reproduction-predicate}">
		<constructor-arg value="${labs.emas.reproduction-predicate-value}"/>
	</bean>

	<bean id="dieEnergyTreshold" class="java.lang.Double">
		<constructor-arg value="${labs.emas.die-energy-threshold}"/>
	</bean>

	<bean id="recombination" class="${labs.emas.recombination}" scope="prototype"></bean>

	<bean id="mutation" class="${labs.emas.mutation}" scope="prototype">
		<constructor-arg name="bitsToMutatePercentage" value="${labs.emas.mutation.bits-to-mutate-percentage}"/>
	</bean>

	<bean id="sexualReproductionEnergyTransfer"
	      scope="prototype"
	      class="pl.edu.agh.age.compute.stream.emas.reproduction.transfer.EnergyTransfer"
	      factory-method="${labs.emas.transfer.sexual-energy-transfer}">
		<constructor-arg value="${labs.emas.transfer.sexual-energy-transfer-value}"/>
		<constructor-arg name="minimumAgentEnergy" ref="dieEnergyTreshold"/>
	</bean>

	<bean id="asexualReproductionEnergyTransfer"
	      scope="prototype"
	      class="pl.edu.agh.age.compute.stream.emas.reproduction.transfer.AsexualEnergyTransfer"
	      factory-method="${labs.emas.transfer.asexual-energy-transfer}">
		<constructor-arg value="${labs.emas.transfer.asexual-energy-transfer-value}"/>
		<constructor-arg name="minimumAgentEnergy" ref="dieEnergyTreshold"/>
	</bean>

	<bean id="fightEnergyTransfer"
	      scope="prototype"
	      class="pl.edu.agh.age.compute.stream.emas.fight.transfer.FightEnergyTransfer"
	      factory-method="${labs.emas.transfer.fight-energy-transfer}">
		<constructor-arg value="${labs.emas.transfer.fight-energy-transfer-value}"/>
		<constructor-arg name="minimumAgentEnergy" ref="dieEnergyTreshold"/>
	</bean>

	<bean id="fightAgentComparator"
	      class="pl.edu.agh.age.compute.stream.emas.EmasAgentComparators"
	      factory-method="higherFitnessProbabilistic"></bean>

	<bean id="deathPredicate"
	      class="pl.edu.agh.age.compute.stream.emas.Predicates"
	      factory-method="energyBelowThreshold">
		<constructor-arg name="energyThreshold" ref="dieEnergyTreshold"/>
	</bean>

	<!-- *************** EMAS PLATFORM CONFIGURATION *************** -->

	<!-- Define logging parameters -->
	<bean id="loggingParameters" class="pl.edu.agh.age.compute.stream.logging.LoggingParameters">
		<constructor-arg name="problemDefinition" ref="problemDefinition"/>
		<constructor-arg name="loggingInterval" ref="loggingInterval"/>
	</bean>

	<!-- Define logging service -->
	<bean id="loggingService" class="pl.edu.agh.age.compute.stream.logging.DefaultLoggingService"/>

	<!-- Define EMAS Agents comparator -->
	<bean id="agentComparator"
	      class="pl.edu.agh.age.compute.stream.emas.EmasAgentComparators"
	      factory-method="higherFitness"></bean>

	<!-- Define EMAS Agents registry -->
	<bean id="agentsRegistry" class="pl.edu.agh.age.compute.stream.emas.EmasBestAgentsRegistry">
		<constructor-arg name="agentComparator" ref="agentComparator"/>
	</bean>

	<!-- LABS Solution Factory -->
	<bean id="solutionFactory" class="pl.edu.agh.age.compute.labs.solution.LabsSolutionFactory" factory-method="random">
		<constructor-arg name="problemDefinition" ref="problemDefinition"/>
		<constructor-arg name="evaluator" ref="evaluator"/>
	</bean>

	<!-- Define agents generator for each workplace -->
	<bean id="agents-generator" class="pl.edu.agh.age.compute.labs.LabsPopulationGenerator">
		<constructor-arg name="solutionFactory" ref="solutionFactory"/>
		<constructor-arg name="agentsCount" ref="agentsCount"/>
		<constructor-arg name="initialAgentEnergy" ref="initialAgentEnergy"/>
	</bean>

	<!-- LABS evaluator counter -->
	<bean id="evaluatorCounter"
	      class="pl.edu.agh.age.compute.stream.problem.EvaluatorCounter"
	      factory-method="simpleCounter"></bean>

	<!-- LABS evaluator -->
	<bean id="evaluator" class="pl.edu.agh.age.compute.labs.evaluator.LabsEvaluator" scope="prototype">
		<constructor-arg name="counter" ref="evaluatorCounter"/>
	</bean>

	<!-- Define population evaluator -->
	<bean id="populationEvaluator"
	      class="pl.edu.agh.age.compute.stream.emas.MemeticPopulationEvaluator"
	      scope="prototype"/>

	<!-- Define a before-step analyzer -->
	<bean id="before-step-analyzer"
	      class="pl.edu.agh.age.compute.stream.BeforeStepAction"
	      scope="prototype"
	      factory-method="simpleMerge"/>

	<!-- Define a step function for workplaces -->
	<bean id="step" class="pl.edu.agh.age.compute.stream.emas.EmasStep" scope="prototype"/>

	<!-- Define an after-step analyzer -->
	<bean id="population-analyzer"
	      class="pl.edu.agh.age.compute.stream.example.SampleAfterStepActionWithLogging"
	      scope="prototype"/>

	<!-- Define workplaces -->
	<bean id="workplace-generator" class="pl.edu.agh.age.compute.stream.configuration.WorkplaceConfigurationGenerator">
		<constructor-arg name="workplacesCount" ref="workplacesCount"/>
		<constructor-arg name="generator" ref="agents-generator"/>
	</bean>

	<!-- Initialize configuration -->
	<bean id="configuration" class="pl.edu.agh.age.compute.stream.configuration.Configuration">
		<constructor-arg name="workplaceConfigurationGenerator" ref="workplace-generator"/>
		<constructor-arg name="stopCondition" ref="stopCondition"/>
		<constructor-arg name="loggingService" ref="loggingService"/>
		<constructor-arg name="topology" ref="topology"/>
	</bean>

	<!-- Define main class -->
	<bean id="runnable" class="pl.edu.agh.age.compute.stream.StreamAgents"/>

</beans>
