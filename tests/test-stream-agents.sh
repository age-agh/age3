#!/bin/bash

set -e

readonly shell_binary=$1

./gradlew age3-stream-agents:build
cp age3-stream-agents/build/libs/age3-stream-agents-0.7-SNAPSHOT.jar ./lib/


cat << EOF > comp-config
computation.load({ jars:['lib/age3-stream-agents-0.7-SNAPSHOT.jar'], config: 'file:age3-stream-agents/src/main/resources/pl/edu/agh/age/compute/stream/example/spring-stream-example.xml' })
sleep(5)
computation.start()
sleep(5)
computation.waitUntilFinished()
EOF

java -jar $shell_binary standalone ./comp-config

